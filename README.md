# Remote interview for MatchX

[![pipeline status](https://gitlab.com/pheix-interviews/matchx/badges/master/pipeline.svg)](https://gitlab.com/pheix-interviews/matchx/commits/master)

## Code challenge

Solved with JavaScript: https://narkhov.pro/pub/matchx/

Browse link above, open **developer console** (f12) and check pre-built tree structure. Add new elements to tree via form on page. Check the `InvalidArgumentException` if trying to add the element with existed name.

First version on 6bca8800ab3972e0cdb7606453368f2b5215e2d3 has only single-root support (#1). It was fixed in b579b0456cf8d3e24499d1a561d9bd183720b6e6.

To add new **root** element just keep [+existed elem+] input blank or add to **not existed** element.

## Face-2-face task

Check: https://codebunk.com/b/312346900/

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
