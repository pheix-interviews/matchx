#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define GPIO_5   5
#define GPIO_6   6
#define GPIO_7   7
#define GPIO_8   8
#define GPIO_9   9
#define GPIO_10  10

extern void * f2f_interview_task(void);

void * osUserThread(void);

int main(int argc, char *argv[]) {
	pthread_t thread;
	//pthread_create(&thread, NULL, (void *)osUserThread, NULL);
	pthread_create(&thread, NULL, (void *)f2f_interview_task, NULL);
	pause();
	return 1;
}

void * osUserThread(void) {
	unsigned int tests = 5;
	unsigned int vectr = 0;
	struct timespec tin = { 0, 100000000 };
	struct timespec tout;
	pthread_t main_thrd = pthread_self();
	printf("***info: led test thread 0x%08x\n", (int)main_thrd);
	printf("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
	for(;;) {
		printf("led %d is ON\n", tests);
		nanosleep(&tin , &tout);
		if ( tests >= 10 ) {
			vectr = 1;
		} else if ( tests <= 5) {
            vectr = 0;
		}
		if (!vectr) {
			tests++;
		} else {
			tests--;
		}
	}
	return (void *)NULL;
}
