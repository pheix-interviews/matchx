// Compiled with: gcc -std=c11 -Wall -pthread -lm

#include <stdio.h>

#define LEN 5

int get_pos(unsigned int iter);

unsigned int init_pos = LEN/2;
unsigned int dest     = ((LEN/2)%2 == 1) ? 1 : 0;

void* f2f_interview_task(void)
{
    int original[LEN] = { 1, 2, 3, 4, 5  };
    //int original[LEN] = { 1, 2, 3, 4, 5, 99 };
    //int original[LEN] = { 1, 223, 32, 54, 15, 9, 19, 76, 44, 100 };
    int sorted[LEN]   = {};
    int iter = 0;
    for(int j=0; j<LEN; j++) {
        printf("%d\n",original[j]);
    }
    printf("------\n");

    int srtd = 0;
    while(!srtd) {
        int max = -1;
        int inx = -1;
        for(int i=0; i<LEN; i++) {
            if(original[i] > max) {
                max = original[i];
                inx = i;
            }
        }
        if(max > -1) {
            int s_inx = get_pos(iter);
            printf("pos %d\n",s_inx);
            original[inx] = -1;
            sorted[s_inx]  = max;
            iter++;
        }
        if(iter == LEN) {
            srtd = 1;
        }
    }
    printf("------\n");
    for(int i=0; i<LEN; i++) {
        printf("%d\n",sorted[i]);
    }
    return 0;
}

int get_pos(unsigned int iter) {
    unsigned int rc = 0;
    if(iter == 0) {
        rc = init_pos;
    } else {
        if( dest == 0 ) {
            rc = init_pos+iter;
            dest = 1;
        } else {
            rc = init_pos-iter;
            dest = 0;
        }
        init_pos = rc;
    }
    return rc;
}

/*int * init(unsigned int len) {
    int * arr;
    arr = calloc(len, sizeof(int));
    return arr;
}*/
