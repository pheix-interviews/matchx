﻿function MtchXQueue() {
    this._oldestIndex = 1;
    this._newestIndex = 1;
    this._storage = {};
}

MtchXQueue.prototype.size = function() {
    return this._newestIndex - this._oldestIndex;
};

MtchXQueue.prototype.enqueue = function(data) {
    this._storage[this._newestIndex] = data;
    this._newestIndex++;
};

MtchXQueue.prototype.dequeue = function() {
    var oldestIndex = this._oldestIndex,
        newestIndex = this._newestIndex,
        deletedData;
    if (oldestIndex !== newestIndex) {
        deletedData = this._storage[oldestIndex];
        delete this._storage[oldestIndex];
        this._oldestIndex++;
        return deletedData;
    }
};

function MtchXNode(data) {
    this.data = data;
    this.parent = null;
    this.children = [];
    this.level = 0;
}

function MtchXTree(data) {
    var node = new MtchXNode(data);
    this._root = [node];
}

MtchXTree.prototype.traverseBF = function(callback) {
    var queue = new MtchXQueue();
    for (var i = 0, length = this._root.length; i < length; i++) {
        queue.enqueue(this._root[i]);
    }
    currentTree = queue.dequeue();
    while(currentTree){
        for (var j = 0, length = currentTree.children.length; j < length; j++) {
            queue.enqueue(currentTree.children[j]);
        }
        callback(currentTree);
        currentTree = queue.dequeue();
    }
};

MtchXTree.prototype.contains = function(callback, traversal) {
    traversal.call(this, callback);
};

MtchXTree.prototype.add = function(data, toData, traversal) {
    var child = new MtchXNode(data),
        parent = null,
        callback = function(node) {
            if (node.data === toData) {
                parent = node;
            }
        };
    this.contains(callback, traversal);
    if (parent) {
        parent.children.push(child);
        child.parent = parent;
        child.level  = parent.level+1;
    } else {
        //throw new Error('NoParentNodeException');
        this._root.push(child);
        child.parent = null;
        child.level  = 0;
    }
};

function findIndex(arr, data) {
    var index;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].data === data) {
            index = i;
        }
    }
    return index;
}

function printTree(t) {
    t.traverseBF(function(node) {
        console.log(
            "-".repeat(node.level) +
            "> " +
            node.data +
            " (parent: " +
            ( node.parent === null ? 'I am root' : node.parent.data ) +
            ")"
        );
    });
}

function checkExisted(t,d) {
    var exists = false;
    t.traverseBF(function(node) {
        if (node.data === d ) {
            exists = true;
        }
    });
    return exists;
}

function addFromForm() {
    var newel = document.forms[0].elements[0].value;
    var toel  = document.forms[0].elements[1].value;
    if ( !checkExisted( tree,newel ) ) {
        tree.add(newel, toel, tree.traverseBF);
        console.log("-".repeat(72));
        printTree(tree);
    } else {
        throw new Error('InvalidArgumentException');
    }
}

var tree = new MtchXTree('A');
tree.add('B', 'A', tree.traverseBF);
tree.add('C', 'A', tree.traverseBF);
tree.add('D', 'C', tree.traverseBF);
tree.add('E', 'C', tree.traverseBF);
tree.add('F', 'C', tree.traverseBF);

printTree(tree);
