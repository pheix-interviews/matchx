package MatchX::Tasks;

use strict;
use warnings;

BEGIN { }

#------------------------------------------------------------------------------

sub new {
    my $class = shift;
    my $self = {
        name       => 'MatchX::Tasks',
        revision   => '1.0',
    };
    bless $self, ref($class) || $class;
    return $self;
}

#------------------------------------------------------------------------------

sub getInfo {
    my $self = shift;
    return  $self->{name} . q{,} . $self->{revision};
}

#------------------------------------------------------------------------------

END { }

1;
