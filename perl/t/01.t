#!/usr/bin/env perl

use diagnostics;
use warnings;
use strict;
use Data::Dumper;
use Test::More qw( no_plan ); # for the is() and isnt() function

use MatchX::Tasks;

#------------------------------------------------------------------------------

my $obj = MatchX::Tasks->new;

ok($obj->getInfo, 'MatchX::Tasks,1.0');
